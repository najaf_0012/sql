-- General Info
select * from sales_data;

-- customer name & year
select customer_name, Year_ From sales_data

-- invoice ID & invoice total
select invoice_id, invoice_total from sales_data;

--customer phone & billing city
select customer_phone, billing_city from sales_data;

-- customer name & year 2002
select year_,customer_name from sales_data where year_=2002 ;

-- shipping company & unit price in 2002
select shipping_company, unit_price, year_ from sales_data where year_=2002;

-- unit price 25% in 3rd month
select unit_price, unit_price*0.25, month_ from sales_data where month_=3;

-- week & income_level & source_channel
select week, income_level, source_channel from sales_data where week=39;

-- all data for 2009 year
select * from sales_data where year_=2009;

-- alld for  2009 year and ordered by customer name
 select * from sales_data where year_=2009 order by customer_name;
